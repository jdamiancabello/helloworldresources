package es.jdamiancabello.helloworldresources;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView tvxPhrase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvxPhrase = findViewById(R.id.tvxPhrase);

        //Se accede a los recursos del SDK a través de android.R
        tvxPhrase.setTextColor(ContextCompat.getColor(this, android.R.color.holo_purple));

        //A continuación se modifica el tipo de fuente del TextView
        Typeface fuente = Typeface.createFromAsset(getAssets(),"pokemon_fire_red.ttf");

        tvxPhrase.setTypeface(fuente);
    }
}
